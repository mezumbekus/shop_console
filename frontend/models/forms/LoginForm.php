<?php

namespace yii\base\Model;

use app\models\User;
use Yii;

class LoginForm extends Model 
{
    public $login;
    public $password;
    
    
    public function rules()
    {
        return [
          ['login','required'],
          ['login','trim'],
          ['password','required'],
          ['password','validatePassword'],
        ];
    }
    
    public function login()
    {
        if($this->validate()){
            
        }
    }
    
    public function validatePassword($attribute)
    {
        $user = User::findByUsername($this->login());
        
        if($user){
            if(!Yii::$app->security->validatePassword($this->password, $user['password']))
            {
                $this->addError($attribute,'Incorrect password');
            }
        }
    }
    
}