<?php
namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;
use console\models\Shop;

class ShopController extends Controller
{
    public function actionIndex($file)
    {
        if(!preg_match('/.*?\.csv/si', $file)){
            echo "Укажите csv файл!\n";
            return ExitCode::DATAERR;
        }
        if (!file_exists($file) || !is_readable($file)) {
            echo "Файл не сущетсвует либо недопустимые права для чтения!\n";
            return ExitCode::DATAERR;
        }
        
        if(($handle = fopen($file, 'r')) !== false)
        {
            fgetcsv($handle);
            while(($row = fgetcsv($handle)) !== false)
            {
                $shop = new Shop($row);
               
                if($shop->validate())
                    $shop->save();
                
            }
            fclose($handle);
        }
        return ExitCode::OK;
    }
    
}