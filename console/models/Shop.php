<?php
namespace console\models;
use yii\base\Model;
use Yii;

class Shop extends Model
{
    public $id = NULL;
    public $title;
    public $regionId;
    public $city;
    public $address;
    public $userId;
    
    public function __construct($data) {
        
        $this->set($data);
    }
    
    public static function tableName() {
        return 'shop';
    }

    public function rules()
    {
        return [
            [['regionId','userId'],'integer'],
            [['title','address','city'],'string'],
            [['regionId','title','city','address','userId'],'required'],

        ];
    }
    
    public function set(Array $data)
    {
        $this->regionId = $data[0];
        $this->title = $this->addslashes($data[1]);
        $this->city = $this->addslashes($data[2]);
        $this->address = $this->addslashes($data[3]);
        $this->userId = $data[4];
    }
    
    public function get()
    {
       return [
         'id' => $this->id,
         'title' => $this->title,
         'city' => $this->regionId,
         'address' => $this->address,
         'userId' => $this->userId,
         'regionId' => $this->regionId,
       ];
    }
    
    public function save()
    {
        
        $sql = "INSERT INTO shop (title,regionId,city,address,userId) "
                . "VALUES(:title,:regionId,:city,:address,:userId)";
        Yii::$app->db->createCommand($sql,[
            ':city' => $this->city,
            ':title' => $this->title,
            ':regionId' => $this->regionId,
            ':address' => $this->address,
            ':userId' => $this->userId,
        ])->execute();
    }


    
  
    
    private function addslashes($str)
    {
        
        return mb_convert_encoding($str, 'utf-8','cp-1251');
    }
}
