<?php

use yii\db\Migration;

/**
 * Class m181018_161448_shop
 */
class m181018_161448_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('shop', [
           'id' => $this->primaryKey(),
           'title' => $this->string()->notNull(),
           'regionId' => $this->integer()->notNull(),
           'city' => $this->string()->notNull(),
           'address' => $this->string()->notNull(),
           'userId' => $this->integer()->notNull(),
            
        ],"CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB");
    }

    
    public function safeDown()
    {
       
        $this->dropTable('shop');

        return false;
    }


}
